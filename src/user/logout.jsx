import React from "react";

class Logout extends React.Component{
    render(){
        return <div className="base-container">
            <div className="header">
                You have successfully logged out !
            </div>
        </div>
    }
}
export default Logout;