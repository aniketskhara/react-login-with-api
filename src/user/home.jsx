import React from "react";
import { Link } from 'react-router-dom'

class Home extends React.Component{
    render(){
        return <div className="base-container">
            <div className="header">
                You have successfully logged in !
            </div>
            <Link to="/logout">Logout</Link>
        </div>
    }
}
export default Home;