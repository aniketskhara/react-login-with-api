import React from "react";
import { connect } from "react-redux";
import { login } from "../actions/login"
import validateInput from "../server/shared/validations/register"
import PropTypes from 'prop-types';
import axios from 'axios';
import Home from '../user/home';


export class Register extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            dept: '',
            location: '',
            email: '',
            password: '',
            password_confirmation: '',
            submitted: false,
            loading: false,
            errors: {}
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    isValid(){
        const{ errors, isValid } = validateInput(this.state);
        if (!isValid){
            this.setState({ errors });
            console.log('reached')
        }
        return isValid;
    }

    onSubmit(e){
        e.preventDefault();
        if (this.isValid()){
            const { first_name, last_name, dept, location, email, password, password_confirmation, errors } = this.props;
            
            const FIRST_NAME = this.state.first_name;
            const LAST_NAME = this.state.last_name;
            const DEPT = this.state.dept;
            const LOCATION = this.state.location;
            const EMAIL = this.state.email;
            const PASSWORD = this.state.password;
            const PASSWORD_CONFIRMATION = this.state.password_confirmation;
            
            axios.post(`http://localhost.lvh.me:3001/users?user[first_name]=${FIRST_NAME}&user[last_name]=${LAST_NAME}&user[dept]=${DEPT}&user[location]=${LOCATION}&user[email]=${EMAIL}&user[password]=${PASSWORD}&user[password_confirmation]=${PASSWORD_CONFIRMATION}`)
            .then(res => {
              console.log(res.data.status);
              alert(res.data.status);
            })

        }
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value });
    }

    render(){

        const { first_name, last_name, dept, location, email, password, password_confirmation, errors, loading } = this.state;

        return <div className="base-container" ref={this.props.containerRef}>
            <div className="header">
                Register
            </div>
            <div className="content">
                <form onSubmit={this.onSubmit}>
                    <div className="form">
                        <div className="form-group">
                            <label htmlFor="first-name">
                                First Name
                            </label>
                            <input type="text" name="first_name" placeholder="first-name" value={first_name} onChange={this.onChange} error={errors.first_name}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="last-name">
                                Last Name
                            </label>
                            <input type="text" name="last_name" placeholder="last-name" value={last_name} onChange={this.onChange} error={errors.last_name}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="dept-name">
                                Dept Name
                            </label>
                            <input type="text" name="dept" placeholder="dept-name" value={dept} onChange={this.onChange} error={errors.dept}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="location">
                                Location
                            </label>
                            <input type="text" name="location" placeholder="location" value={location} onChange={this.onChange} error={errors.location}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">
                                Email Address
                            </label>
                            <input type="text" name="email" placeholder="Email" value={email} onChange={this.onChange} error={errors.email}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">
                                Password
                            </label>
                            <input type="password" name="password" placeholder="Password" value={password} onChange={this.onChange} error={errors.password}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">
                                Password Confirmation
                            </label>
                            <input type="password" name="password_confirmation" placeholder="Password" value={password_confirmation} onChange={this.onChange} error={errors.password_confirmation}/>
                        </div>
                    </div>
                    <div className="footer">
                        <button type="submit" className="btn" disabled={loading} >Register</button>
                    </div>
                </form>
            </div>
        </div>
    }
}
function mapStateToProps(state){
    return {
       login: PropTypes.func.isRequired
    };
   }
   
export default connect(mapStateToProps)(Register);