import validator from 'validator';
import isEmpty from 'lodash.isempty';

export default function validateInput(data){
    let errors = {};

    if (!validator.isEmail(data.email)){
        errors.email = 'Please enter email address';
    }
    
    if (validator.isEmpty(data.password)){
        errors.password = 'Please enter your password';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };

}