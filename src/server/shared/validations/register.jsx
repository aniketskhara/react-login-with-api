import validator from 'validator';
import isEmpty from 'lodash.isempty';

export default function validateInput(data){
    let errors = {};

    if (!validator.isEmail(data.email)){
        errors.email = 'Please enter email address';
    }
    
    else if (validator.isEmpty(data.password)){
        errors.password = 'Please enter your password';
    }

    else if (validator.isEmpty(data.password_confirmation)){
        errors.password_confirmation = 'Please enter your password';
    }
    
    else if (validator.isEmpty(data.first_name)){
        errors.first_name = 'Please enter your first name';
    }

    else if (validator.isEmpty(data.last_name)){
        errors.last_name.email = 'Please enter your last name';
    }
    
    else if (validator.isEmpty(data.dept)){
        errors.dept = 'Please enter your dept';
    }
    
    else if (validator.isEmpty(data.location)){
        errors.location = 'Please enter your location';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };

}