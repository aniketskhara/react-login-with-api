import React from "react";
import { connect } from "react-redux";
import { login } from "../actions/login"
import validateInput from "../server/shared/validations/login"
import PropTypes from 'prop-types';
import axios from 'axios';
import Home from '../user/home';
import { Redirect } from "react-router-dom"

export class Login extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            submitted: false,
            loading: false,
            errors: {},
            loggedIn: null
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);

    }


    isValid(){
        const{ errors, isValid } = validateInput(this.state);
        if (!isValid){
            this.setState({ errors });
            console.log('reached')
        }
        return isValid;
    }

    onSubmit(e){
        e.preventDefault();
        if (this.isValid()){
            const { email, password } = this.props;
            const EMAIL = this.state.email;
            const PASSWORD = this.state.password;
            axios.post(`http://localhost.lvh.me:3001/users/sign_in?user[email]=${EMAIL}&user[password]=${PASSWORD}`)
            .then(res => {
              console.log(res.data.status);
              if (res.data.status == 'Login Successful'){
                  this.setState({
                    loggedIn: true
                  })
              }
              else{
                alert(res.data.status);
              }
            })

        }
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value });
    }

    render(){
        const { email, password, loading, errors } = this.state;
        if (this.state.loggedIn){
            return <Redirect to="/home" />
        }
        return <div className="base-container" ref={this.props.containerRef}>
            <div className="header">
                Login
            </div>
            <form onSubmit={this.onSubmit}>
                <div className="content">
                    <div className="form">
                        <div className="form-group">
                            <label htmlFor="email">
                                Email Address
                            </label>
                            <input type="text" name="email" value={email} placeholder="Email" onChange={this.onChange} error={errors.email} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">
                                Password
                            </label>
                            <input type="password" name="password" value={password} placeholder="Password" onChange={this.onChange} error={errors.password} />
                        </div>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn" disabled={loading} >Login</button>
                    </div>
                </div>
            </form>
        </div>
    }
}

function mapStateToProps(state){
 return {
    login: PropTypes.func.isRequired
 };
}

export default connect(mapStateToProps)(Login);