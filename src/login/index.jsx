import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import Home from '../user/home';
import Logout from '../user/logout';
import "./style.scss";
export { Login } from "./login";
export { Register } from "./register";


export default function AuthExample() {
    return (
      <Router>
          <Switch>
            <Route path="/home" component={Home} />
            <Route path="/logout" component={Logout} />
          </Switch>
      </Router>
    );
  }
  